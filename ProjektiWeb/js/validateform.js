$(document).ready(function() {
    
    $("#signup-form").submit(function(event) {

        event.preventDefault();

        var first = $("#first").val();
        var last = $("#last").val();
        var email = $("#email").val();
        var pwd = $("#pwd").val();
        var pwd2 = $("#pwd2").val();
        var gender = '';
        if ($("#genderM").prop('checked')){
            gender = $("#genderM").val();
        }else if ($("#genderF").prop('checked')) {
            gender = $("#genderF").val();
        }
        var contry = $("#contry").val();
        var birthday = $("#birthday").val();
        var is_admin = $("#is_admin").prop('checked') ? is_admin = 1 : is_admin = 0;
        var submit = $("#submit").val();

        $("#form-message").load("./includes/signup.php", {
            first: first,
            last: last,
            email: email,
            pwd:   pwd,
            pwd2: pwd2,
            gender: gender,
            contry: contry,
            birthday: birthday,
            is_admin: is_admin,
            submit: submit
        });
    });
});