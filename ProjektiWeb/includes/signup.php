 <?php
 
    include 'db.php';
    
    if(isset($_POST['submit'])){

        $first = $_POST['first'];
        $last = $_POST['last'];
        $email = $_POST['email'];
        $pwd = $_POST['pwd'];
        $pwd2 = $_POST['pwd2'];
        $gender = $_POST['gender'];
        $contry = $_POST['contry'];
        $birthday = $_POST['birthday'];
        $isAdmin = $_POST['is_admin'];

        $errorEmpty = $errorFirst =  $errorlast =  $erroremail = $errorBirthday =  $errorPassword =   $successSignup = false;

        if(empty($_POST['first']) || empty($_POST['last']) || empty($_POST['email']) || empty($_POST['gender'])   || empty($_POST['birthday']) || empty($_POST['pwd']) || empty($_POST['pwd2'])){
        
            echo '<span class="error">Empty Fileds</span>';
            $errorEmpty = true;

        }elseif(!preg_match("/^[a-zA-Z]*$/", $_POST['first'])){
            
            echo '<span class="error">Empty Fileds</span>';
            $errorFirst = true;
            

        }elseif(!preg_match("/^[a-zA-Z]*$/", $_POST['last'])){

            echo '<span class="error">Empty Fileds</span>';
            $errorlast = true;

        }elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                
            echo '<span class="error">Type a Valid Email!</span>';
            $erroremail = true;
                    
        }elseif(empty($_POST['birthday'])){

            echo '<span class="error">Fiel Birthday</span>';
            $errorBirthday = true;

        }elseif($_POST['pwd'] !== $_POST['pwd2']){

            echo '<span class="error">Password doesnt match</span>';
            $errorPassword = true;

    }else{
        $query = $pdo->prepare("SELECT email FROM users WHERE email= :email");
        $query->execute([':email' => $email]);
        $users = $query->fetch();

        if ($users > 0) {
            
            echo '<span class="error">Email is Used Sorry</span>';
            $erroremail = true;

        }else{
        $password = password_hash($pwd, PASSWORD_DEFAULT);

        $query = $pdo->prepare('INSERT INTO users (first, last, email, gender, birthday, contry, password, is_admin) 
        VALUES (:first, :last, :email, :gender, :birthday, :country, :password, :is_admin)');
                
        // $query->bindParam(':first',$_POST['first']);
        // $query->bindParam(':last',$_POST['last']);
        // $query->bindParam(':email',$_POST['email']);
        // $query->bindParam(':pwd',$password);
        // $query->bindParam(':gender',$_POST['gender']);
        // $query->bindParam(':birthday',$_POST['birthday']);
        // $query->bindParam(':contry',$_POST['contry']);
        // $query->bindParam(':is_admin',$isAdmin);
            
        $query->execute([
            ':first' => $first,
            ':last' => $last,
            ':email' => $email,
            ':password' => $password,
            ':gender' => $gender,
            ':birthday' => $birthday,
            ':country' => $contry,
            ':is_admin' => $isAdmin
        ]);
      
        echo '<span class="success">You have Sign Up</span>';
        $successSignup = true;

        }
    }
                
}
      

?>

<script>

    $("#first, #last, #email, #pwd, #pwd2, #gender, #birthday").removeClass("input-error");

    if("<?php echo $errorEmpty; ?>" == true){

        $("#first, #last, #email, #pwd, #pwd2, #birthday, #gender").addClass("input-error");

    }
    if("<?php echo $errorFirst; ?>" == true){
        
        $('#first').addClass("input-error");
    }
    if("<?php echo $errorlast; ?>" == true){

        $('#last').addClass("input-error");
    }
    if("<?php echo $erroremail; ?>" == true){

    $('#email').addClass("input-error");

    }
    if("<?php echo $errorBirthday; ?>" == true){

    $('#birthday').addClass("input-error");

    }
    if("<?php echo $errorPassword; ?>" == true){

    $('#pwd').addClass("input-error");

    }

</script>