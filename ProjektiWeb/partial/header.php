<?php
    session_start();
?>

<!DOCTYPE html>
<html >

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Projekti</title>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>  -->
    <script  src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="js/loadnews.js"></script>
    <script src="js/validateform.js"></script>
    <script src="js/search.js"></script>
    <link rel="stylesheet" href="css/projekt.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/slider.css">
</head>

<body>
    <div class="container-1">
        <div class="header">
            <div class="headName">
                <h1 class="headName-1">Your Blog Name</h1>
                <h3 class="headName-2">Just another  Blog website</h3>
            </div>
            <div class="logform">
                <?php 
                if(isset($_SESSION['id'])){
                    echo '<form action="includes/logout.php" method="POST"> 
                            <button type="submit" name="submit" class="logout-button">Logout</button>
                          </form>';
                }else{
                    echo '<form class="formbox" action="includes/login.php" method="POST">
                            <div class="divlogi">
                                <label for="label1">                            `
                                    <input type="checkbox" class="logbox-1" ><em>Keep me logged in</em>
                                </label>
                                <br>
                                <input type="text" name="email" class="logbox-1" id="label1" placeholder="Email">
                                <p id="textunderform"><em>Not a member?<a href="register.php" class="ngjyraTextit">Register</a> for free.</em></p>
                            </div>
                            <div class="divlogi-1">
                                <p id="textilogit"><em>Forgot <a href="#" class="ngjyraTextit">Password</a>?</em></p>
                                <input type="password" name="pwd" placeholder="Password" >
                                <button type="submit" name="submit" class="login-button">Log in</button>
                            </div>
                          </form>';
                }
                ?>
                <?php 

                    if(isset($_GET['error']) && $_GET['error'] == 'user-pw-wrong'){
                        echo '<p class="error">Wrong user or Password</p>';
                    }

                ?>
                
            </div>
            <p id="search-result"></p>
        </div>
        <div class="navbar">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Solutions</a></li>
                <li><a href="#">Alliances</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="contact.php">Contact Us</a></li>
                <?php 
                    if(isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == '1'){
                     echo '<li><a href="admin.php">Users</a></li>';
                     echo '<li><a href="addnews.php">Add News</a></li>';
                    }
                ?>
                
            </ul>
            <div class="search-container">
                <form action="search.php" method="POST" id="search">
                    <input type="text" placeholder="Search" name="search" id="search-input">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>