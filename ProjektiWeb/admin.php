<?php include 'partial/header.php' ?>
<?php include 'includes/db.php' ?>
<?php include 'includes/admincheck.php' ?>
    
<?php

    $query = $pdo->query('SELECT * FROM users');
    $users = $query->fetchAll();

?>


 <div class="container">
    <?php if(isset($_SESSION['is_admin']) && $_SESSION['is_admin'] === '1'): ?>
        <table border='1'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                   
                        <th>Actions</th>
                    
                </tr>
            </thead>
            <?php endif; ?>
            <tbody>
                <?php foreach($users as $user): ?>
                    <tr>
                        <td><?php echo $user['id']; ?></td>
                        <td><?php echo $user['first']; ?></td>
                        <td><?php echo $user['email']; ?></td>
                        <td>
                            <?php if(isset($_SESSION['is_admin']) && $_SESSION['is_admin'] === '1'): ?>
                                <a href="edit.php?id=<?php echo $user['id']; ?>">Edit</a>
                                <a href="delete.php?id=<?php echo $user['id']; ?>">Delete</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>






 <?php include 'partial/footer.php' ?>


















