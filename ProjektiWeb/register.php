<?php include 'partial/header.php' ?>

<div class="container">
<div class="main-wrapper">
    <h1>Sign-UP</h1>
   
    
    <form class="signup-form" action="includes/signup.php" method="POST" id="signup-form">
        <div class="form-name">
            <p id="form-message"></p>
            <input type="text" name="first" id="first" placeholder="Emri" autocomplete="off">
            <input type="text" name="last" id="last" placeholder="Mbiemri" autocomplete="off">
            <input type="text" name="email" id="email" placeholder="📧Email"require>
            <div class="gender-form">
            <label for="gender"  class="ngjyraTextit">Gjinia</label><br>
            <input type="radio" name="gender" id="genderM" value="M" checked="checked">Male
            <input type="radio" name="gender" id="genderF" value="F"/>Female
        </div>
        <label for="data"  class="ngjyraTextit">Viti-Lindjes</label>
        <input type="date" id="birthday" name="birthday" >
        <label for="data" class="ngjyraTextit">Zgjidhni Shtetin: </label><br>
        <select name="contry" id="contry">
            <option value="Kosova">Kosova</option>
            <option value="Albania">Albania</option>
            <option value="United States">United States</option>
            <option value="Switzerland">Switzerland</option>
        </select>
        <input type="password" id="pwd" name="pwd" placeholder="🔓Password"></i>
        <input type="password" id="pwd2" name="pwd2" placeholder="🔓Repeat-Password">
        <input type="checkbox" name="is_admin" class="checkBox" id="is_admin"><p>Is Admin<p><br>
        <div id="newsbutton">
            <button type="submit" name="submit" id="submit">Sign Up</button>
        </div>
    </form>
</div>
</div>


<?php include 'partial/footer.php' ?>