    <?php  include 'partial/header.php' ?>
        <div class="kontenti">
            <div class="tekstiKontentit">
                <h2 id="ngj">LATES PROJECT</h2>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aspernatur cumque dicta voluptatem fugit voluptatum. Dicta natus saepe enim cumque odit officia aut consequatur porro nostrum unde. Sed maiores ullam illum? Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere tenetur quidem rem id, reprehenderit, hic voluptas labore, deserunt non nesciunt eos libero. Provident harum doloremque sit sequi dicta? Saepe, mollitia!
                </p>
                <div class="btn">
                    <button class="btn-1"><img src="img/viewmore.png" alt="picture" class="img-button"></button>
                </div>
                <h3 id="viewmore-11">VIEW PROJECT</h3>
            </div>
            <div class="slider">
                <div class="slideshow-container">
                    <div class="myslides fade">
                        <div><img src="img/slider1.png" class="img-div" alt="sliderimg"></div>
                    </div>
                    <div class="myslides fade">
                        <div><img src="img/slider1.png" class="img-div" alt="sliderimg"></div>
                    </div>
                    <div class="myslides fade">
                        <div><img src="img/slider1.png" class="img-div" alt="sliderimg"></div>
                    </div>
                    <div class="myslides fade">
                        <div><img src="img/slider1.png" class="img-div" alt="sliderimg"></div>
                    </div>
                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>
                </div>
                <br>
                <div class="dots-1">
                    <span class="dots" onclick="currentSlide(1)"></span>
                    <span class="dots" onclick="currentSlide(2)"></span>
                    <span class="dots" onclick="currentSlide(3)"></span>
                    <span class="dots" onclick="currentSlide(4)"></span>
                </div>
            </div>
        </div>
        <div class="slider2">
            <p>
                <a href="#"><img src="img/slider2icon/1st.gif" alt="slider2" align="middle">Loremorem ispum</a>
            </p>
            <p>
                <a href="#"><img src="img/slider2icon/2st.gif" alt="slider2" align="middle">Lorem</a>
            </p>
            <p>
                <a href="#"><img src="img/slider2icon/3st.gif" alt="slider2" align="middle">Lorem</a>
            </p>
            <p>
                <a href="#"><img src="img/slider2icon/4st.gif" alt="slider2" align="middle">Lorem</a>
            </p>
        </div>
        <div class="bottommid">
            <div class="mid">
            <?php include 'includes/db.php' ?>
            <?php   
                //query per count te faqev
                $query = $pdo->query("SELECT * FROM news ORDER BY id DESC LIMIT 0,2");
                $queryfaqev = $pdo->query("SELECT * FROM news ORDER BY id DESC ");
                $newsfaqev = $queryfaqev->fetchAll();
                $page1;
                if(isset($_GET['page'])){
                    
                    $page = $_GET['page'];
                    
                    if($page == "" || $page == "1"){
                        
                        $page1 = 0;
                        
                    }else{
                        
                        $page1 = ($page*2)-2;
                    }
                    $query = $pdo->query("SELECT * FROM news ORDER BY id DESC LIMIT $page1,2");
                }
                
                //query per index
                $news = $query->fetchAll();
                $a = count($newsfaqev)/2;
                $a = ceil($a);
               
            ?>  
            <?php foreach($news as $new): ?>
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-header-h"><?php echo $new['title'] ?></h2>
                        <a href="#">Comments(20)</a>
                        <p class="card-header-p">June 15 in programing by <?php echo $new['Author'] ?></p>
                    </div>
                    <div class="card-img">
                        <img src="newsfoto/<?php echo $new['img'] ?>" class="news-img" alt="">
                    </div>
                    <div class="card-text">
                        <p><?php echo substr($new['Text'], 0, 200) ?>...</p>
                        <br>
                        <br>
                        <a href="search.php?id=<?php echo $new['title'] ?>">Read More</a>
                    </div>
                </div>
                <?php endforeach; ?>
           
            </div>
            <div class="left-mid">
                <div class="left-mid1">
                    <a href="#"><img src="img/mid-icon/1st.gif" alt=""></a>
                    <a href=""><img src="img/mid-icon/2st.gif" alt=""></a>
                    <a href=""><img src="img/mid-icon/3st.gif" alt=""></a>
                    <a href=""><img src="img/mid-icon/4st.gif" alt=""></a>
                    <a href=""><img src="img/mid-icon/5st.gif" alt=""></a>
                </div>
                <h3 id="h3-mid">Stay connectet With US</h3>
                <a href="#"><img src="img/mid-icon/6st.gif" id="left-mid-readers" alt=""></a>
            </div>
            <div class="left-recent">
                <div class="recent-title">
                    <h3>Recent Post</h3>
                </div>
                <div class="recent-list">
                    <ul>
                    <?php
                      $query = $pdo->query('SELECT * FROM news ORDER BY id DESC LIMIT 5');
                      $news = $query->fetchAll();
                      ?>    
                        <?php foreach($news as $new): ?>
                        <li><a href="search.php?id=<?php echo $new['title'] ?>"><img src="img/mid-icon/arrow.gif" class="arrow" alt=""><?php echo $new['title']?></a></li>
                        <!-- <li><a href="#"><img src="img/mid-icon/arrow.gif" class="arrow" alt="">Lorem ipsum dolor sit amet consectetur</a></li> -->
                        <!-- <li><a href="#"><img src="img/mid-icon/arrow.gif" class="arrow" alt="">Lorem ipsum dolor sit amet consectetur</a></li> -->
                        <!-- <li><a href="#"><img src="img/mid-icon/arrow.gif" class="arrow" alt="">Lorem ipsum dolor sit amet consectetur</a></li> -->
                        <!-- <li><a href="#"><img src="img/mid-icon/arrow.gif" class="arrow" alt="">Lorem ipsum dolor sit amet consectetur</a></li> -->
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="left-recent">
                <div class="recent-title">
                    <h3>Recent Comments</h3>
                </div>
                <div class="recent-list">
                    <ul>
                        <li><a href="#"><img src="img/mid-icon/comments.gif" class="arrow" alt="" align="middle">Wow...!this is amazing...by admin on Post 1</a> </li>
                        <li><a href="#"><img src="img/mid-icon/comments.gif" class="arrow" alt="" align="middle">Wow...!this is amazing...by admin on Post 1</a></li>
                        <li><a href="#"><img src="img/mid-icon/comments.gif" class="arrow" alt="" align="middle">Wow...!this is amazing...by admin on Post 1</a></li>
                        <li><a href="#"><img src="img/mid-icon/comments.gif" class="arrow" alt="" align="middle">Wow...!this is amazing...by admin on Post 1</a></li>
                        <li><a href="#"><img src="img/mid-icon/comments.gif" class="arrow" alt="" align="middle">Wow...!this is amazing...by admin on Post 1</a></li>
                    </ul>
                </div>
            </div>
            <div class="mid-slider">
            <?php for($b=1;$b<=$a;$b++):?>
                        
                        <a href="index.php?page=<?php echo $b; ?>"><?php echo $b; ?></a>
                    <?php endfor; ?>
            
            </div>
        </div>

    <?php include 'partial/footer.php' ?>
    
       