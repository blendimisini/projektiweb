<?php include 'includes/db.php' ?>

<?php 
    $query = $pdo->prepare('DELETE FROM news WHERE id = :id');
    $query->bindParam(':id', $_GET['id']);
    $query->execute();
    
    header('Location: ./news.php');