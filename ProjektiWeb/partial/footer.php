<div class="footer-list">
            <div class="footer-info">
                <h3 class="text">Site Map</h3>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About Me</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Service</a></li>
                </ul>
            </div>
            <div class="footer-info">
                <h3 class="text">Archives</h3>
                <ul>
                    <li><a href="#">April 2010</a></li>
                    <li><a href="#">May 2010</a></li>
                    <li><a href="#">June 2010</a></li>
                    <li><a href="#">July 2010</a></li>
                </ul>
            </div>
            <div class="footer-info">
                <h3 class="text">Get Support</h3>
                <ul>
                    <li><a href="#">FAQs</a></li>
                    <li><a href="#">Contact Me</a></li>
                    <li><a href="#">Call Today:</a></li>
                    <li><a href="#"></a></li>
                </ul>
            </div>
            <div class="footer-info">
                <h3 class="text">Talk to Me</h3>
                <ul>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Newsletter</a></li>
                    <li><a href="#">Rss</a></li>
                </ul>
            </div>
        </div>
        <p id="copyright"> <?php 
        echo "@CopyRight".date("Y"); ?></p>

    </div>
    <script src="js/slider.js"></script>
</body>

</html>