<?php include 'partial/header.php' ?>
<?php include 'includes/db.php' ?>
<?php include 'includes/admincheck.php' ?>
<?php 
    $query = $pdo->prepare('SELECT * FROM users WHERE id = :id');
    $query->execute([':id'=> $_GET['id']]);

    $user = $query->fetch();
    

    if(isset($_POST['submit'])){
        $query = $pdo->prepare('UPDATE users  SET first = :first , last = :last , email = :email , birthday = :birthday, contry = :contry, is_admin = :is_admin where id=:id');

        $query->bindParam(':first',$_POST['first']);
        $query->bindParam(':last',$_POST['last']);
        $query->bindParam(':email',$_POST['email']);
        $query->bindParam(':birthday',$_POST['birthday']);
        $query->bindParam(':contry',$_POST['contry']);
        $query->bindParam(':is_admin',$_POST['is_admin']);
        $query->bindParam(':id',$_GET['id']);
    
        $query->execute();
    }
?>
<div class="container">
<div class="main-wrapper">
<form class="signup-form" action="" method="POST">
    <div class="form-name">
        <input type="text" name="first" placeholder="Emri" autocomplete="off" value="<?php echo $user['first']; ?>">
        <input type="text" name="last" placeholder="Mbiemri" autocomplete="off" value="<?php echo $user['last'];?>">
        <input type="text" name="email" placeholder="📧Email" require value="<?php echo $user['email']; ?>">
        <label for="data" class="ngjyraTextit" >Viti-Lindjes</label>
        <input type="date" name="birthday" value="<?php echo $user['birthday'];?>">
        <label for="data" class="ngjyraTextit" >Zgjidhni Shtetin: </label><br>
        <select name="contry">
            <option value="Kosova" <?php if($user['contry']=='Kosova'){  echo 'selected'; }?> >Kosova</option>
            <option value="Albania" <?php if($user['contry']=='Albania'){  echo 'selected';} ?>>Albania</option>
            <option value="United States" <?php if($user['contry']=='United States'){  echo 'selected';} ?>>United States</option>
            <option value="Switzerland" <?php if($user['contry']=='Switzerland'){  echo 'selected'; }?>>Switzerland</option>
        </select><br><br>   
        <label for="is_admin">Make admin</label><br><br>
             <select name="is_admin" id="is_admin">
                <option value="0" <?php if($user['is_admin']=='0'){  echo 'selected'; }?>>User</option>
                <option value="1" <?php if($user['is_admin']=='1'){  echo 'selected'; }?>>Admin</option>
            </select><br><br>
        <button type="submit" name="submit">update</button>
    </form>

</div>
</div>


<?php include 'partial/footer.php' ?>