-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2019 at 11:13 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loginsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `Author` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `Text` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `time_upload` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `Author`, `title`, `Text`, `img`, `time_upload`) VALUES
(27, 'Blendi', 'GOLF', 'Volkswagen Group, also called Volkswagen AG, major German automobile manufacturer, founded by the German government in 1937 to mass-produce a low-priced â€œpeopleâ€™s car.â€ Headquarters are in Wolfsburg, Germany.\r\n\r\nThe company was originally operated by the German Labour Front (Deutsche Arbeitsfront), a Nazi organization. The Austrian automotive engineer Ferdinand Porsche, who was responsible for the original design of the car, was hired by the German Labour Front in 1934, and ground was broken for a new factory in the state of Lower Saxony in 1938. The outbreak of World War II in 1939 occurred before mass production could begin, and the factory was repurposed to produce military equipment and vehicles. Volkwagenâ€™s military involvement made its factory a target for Allied bombers, and by the end of the war the factory was in ruins. It was rebuilt under British supervision, and mass production of the Volkswagen began in 1946. Control of the company was transferred in 1949 to the West German government and the state of Lower Saxony. By that time, more than half of the passenger cars produced in the country were Volkswagens.', 'golf.jpg', '2019-06-19 21:25:24'),
(28, 'Blendi', 'Audi', 'AUDI AG is a German company which produces cars under the Audi brand. It is part of the Volkswagen Group. The name Audi is based on a Latin translation of the surname of the founder August Horch, itself the German word for â€œlisten!\" Audi is headquartered in Ingolstadt, Germany.\r\n\r\nOn July 16, 1909 automotive pioneer August Horch founded August Horch Automobilwerke GmbH in Germany. A short time later he renamed the company after the Latin translation of his last name â€“ making it Audi Automobilwerke.\r\n\r\nIn 1932, Audi merged with Horch, DKW and Wanderer, to form Auto Union AG. The four rings of the Audi badge symbolize the brands Audi, DKW, Horch and Wanderer. Before World War II, Auto Union AG used the four interlinked rings, only on Auto Union racing cars in that period. Member companies used their own names and emblems.\r\n\r\nAuto Union GmbH was established in Ingolstadt on September 3rd, 1949 following a series of changes as WWII came to an end.\r\n\r\nDaimler-Benz AG acquired the majority of and finally the remaining shares in Auto Union GmbH on April 24th, 1958. From this date until the end of 1965, Auto Union was a fully owned subsidiary of the Stuttgart-based Daimler Group.\r\n\r\nVolkswagenwerk AG acquired the majority of shares in Auto Union GmbH in December 1964, with Audi becoming a fully owned VW subsidiary from the end of 1966.\r\n\r\nIn March 1969, NSU Motorenwerke AG, which had just been taken over by VW, and the Ingolstadt-based Auto Union GmbH merged to form Audi NSU Auto Union AG.\r\n\r\nIn March 1980, a four-wheel-drive sports coupÃ© was introduced at the Geneva Motor Show. The Audi Quattro was the first high-performance vehicle with four-wheel drive. This drive concept had previously only been used on trucks and off-road vehicles. The permanent four-wheel-drive system in the Audi Quattro enjoyed worldwide success in motor sport and gradually found its way into the entire Audi model range.\r\n\r\nIn January 1985, Audi NSU Auto Union AG was renamed AUDI AG. At the same time the company moved its head office from to Ingolstadt. From this time forward automobiles and the company had the same name.', 'audi.jpg', '2019-06-19 21:26:12'),
(29, 'Blendi', 'Mercedes Benz', 'Gottlieb Daimler is born on 17 March 1834 in Schorndorf. After training as a gunsmith and working in France, he attends the Polytechnic School in Stuttgart from 1857 to 1859. After completing various technical activities in France and England, he starts work as a draftsman in Geislingen in 1862. At the end of 1863, he is appointed workshop inspector in a machine tool factory in Reutlingen, where he meets Wilhelm Maybach in 1865. In 1872, he becomes Technical Director of the gas engine manufacturer Deutz Gasmotorenfabrik, where he becomes familiar with Ottoâ€™s four-stroke technology. After differences with the Managing Director, he leaves the company in 1882. Daimler sets up a development workshop in his greenhouse at his Cannstatt villa to concentrate on developing petrol-driven four-stroke engines. Working with Wilhelm Maybach in 1884, he develops an internal combustion engine known today as the \'Grandfather Clock\'. With its compact, low-weight design, the machine forms the basis for installation in a vehicle. The costs of trial operations soon consume Daimlerâ€™s entire fortune, however, so he is obliged to find business partners. He founds \'Daimler-Motoren-Gesellschaft\' on 28 November 1890 together with Max Duttenhofer and his business partner Wilhelm Lorenz. But while Duttenhofer wants to produce stationary engines, Daimler prefers to focus on vehicle production, and a dispute ensues.', 'benzi1.jpg', '2019-06-19 21:27:12'),
(30, 'Blendi', 'BMW', 'BMW has been one of the most active and successful brands in motor sport from day one, notching up countless victories and records on two, three and four wheels. BMW Group Classicâ€™s Historic Motorsport division sends its old racing cars and motorcycles back out into the worldâ€™s courses and circuits with the same enthusiasm as the works and privateer teams  back in the day.', 'bmw.jpg', '2019-06-19 21:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first` varchar(255) NOT NULL,
  `last` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `contry` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first`, `last`, `email`, `gender`, `birthday`, `contry`, `password`, `is_admin`, `time`) VALUES
(8, 'Blendi', 'Misini', 'blendimisini@gmail.com', 'M', '1999-04-27', 'United States', '$2y$10$lGQasYlSJxfMD.qwGpXek.E1yG/Wec7Zm9zQWV0PQbT9BOjJCH8Bu', 1, '2019-06-13 18:50:29'),
(9, 'Qendrim', 'Vrella', 'qendrimvrealla@gmail.com', 'M', '1998-01-01', 'Kosova', '$2y$10$stJUmybwrWWDkKsuU0JeB.WdmBPP/DlV.WhhiCV6udLYx2c6kSb7e', 0, '2019-06-15 18:11:20'),
(11, 'Muhamed', 'Sherifi', 'muhamet.sherifi@ubt-uni.net', 'M', '1997-01-02', 'Switzerland', '$2y$10$itLfxXWRpuJrztBIojsOuu3x.0HgqZ6Eg5O4zbwIfgcQTINc.zhfS', 1, '2019-06-18 16:37:53'),
(12, 'Avni', 'Misini', 'avniu.m@live.com', 'M', '1968-04-14', 'Kosova', '$2y$10$xLmhT5nb/DSoMRgFbtKFje9RI8Z2lhktN1pPojlX05h2MDaJ7X/Y.', 1, '2019-06-19 13:00:53'),
(18, 'Mergim', 'Fejza', 'mergimfejza@gmail.com', 'M', '1999-02-19', 'Kosova', '$2y$10$Pa73hQfcRbbOeauqEO6RoOvQxqN//isibyZeDtjxSqr42S9mYqZMy', 1, '2019-06-20 22:38:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
