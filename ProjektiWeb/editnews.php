
<?php include 'partial/header.php' ?>
<?php include 'includes/admincheck.php' ?>
<?php include 'includes/db.php' ?>

<?php 

    $query = $pdo->prepare('SELECT * FROM news WHERE id = :id');
    $query->bindParam(':id', $_GET['id']);
    $query->execute();    
    $news = $query->fetch();
?>

<?php


if(isset($_POST['submit'])){
    $title = $_POST['title'];
    $text = $_POST['image_text'];
    $image = $_FILES["image"]['name'];
    
   
            $query = $pdo->prepare('UPDATE  news SET Author = :Author , title = :title ,text = :text, img = :img WHERE id = :id'); 
            $query->bindParam(':Author', $_SESSION['first']);
            $query->bindParam(':title', $title);
            $query->bindParam(':text', $text);
            $query->bindParam(':img',$image);
            $query->bindParam(':id', $_GET['id']);
            $query->execute();
             $target = "newsfoto/".basename($image);
             if(move_uploaded_file($_FILES['image']['tmp_name'], $target)){
                 header('Location: editnews.php?newsadd=success');
                 exit();
             }else{
                header('Location: editnews.php?newsadd=fail');
                exit();
             }
}
  
?>

<div class="container-1">
    <div class="upload-form">
        <form action="" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="size" value="1000000">
            
            <br>
            Shkruaj Titullin e Lajmit:<br>
            <input type="text" name="title" value="<?php echo $news['title'] ?> ">
            <br>
            <br>
            <textarea name="image_text"  cols="50" rows="15" placeholder="Shkruaj lajmin." ><?php echo $news['Text'] ?></textarea>
            <br>
            Upload Photo:<br>
            <input type="file" name="image" ><span> <?php echo $news['img'] ?>: Pleas upload again a photo</span><br>
            <br>
            <input type="submit" name="submit"><br><br>

            
        </form>
    </div>
</div>

<?php include 'partial/footer.php' ?>