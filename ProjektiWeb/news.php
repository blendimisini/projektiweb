<?php include 'includes/db.php' ?>
<?php include 'partial/header.php' ?>
<?php   
    $query = $pdo->query('SELECT * FROM news ORDER BY id DESC LIMIT 2');
    $news = $query->fetchAll();
    ?>  
    <?php if(count($news) > 0): ?>
        <?php foreach($news as $new): ?>
                <div id="lajmet">
                <div class="card" id="newspage">
                    <div class="card-header">
                        <h2 class="card-header-h"><?php echo $new['title']; ?></h2>
                        <a href="#">Comments(20)</a>
                        <p class="card-header-p">June 15 in programing by <?php echo $new['Author']; ?></p>
                    </div>
                    <div class="card-img">
                        <img src="newsfoto/<?php echo $new['img']; ?>" class="news-img" alt="Lajmi" >
                    </div>
                    <div class="card-text">
                        <p> 
                            <?php echo $new['Text']; ?>
                        </p>
                    <?php if(isset($_SESSION['is_admin']) && $_SESSION['is_admin'] === "1"): ?>

                        <a href="editnews.php?id=<?php echo $new['id'];?>">Edit</a>
                        <a href="deletenews.php?id=<?php echo $new['id'];?>">Delete</a>
    
                <?php endif; ?>
                    </div>
                </div>
                </div>
        <?php endforeach; ?>
<?php endif; ?>
    <div id="newsbutton">

        <button id="showmore">Show More</button>
        
    </div>
<?php include 'partial/footer.php' ?>